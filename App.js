import React, {Component} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import PlaceItem from "./src/components/PlaceItem";

import {connect} from 'react-redux';
import {addPlace} from "./src/store/actions";


class App extends Component {

    componentDidMount() {

        this.props.addPlace();
    }

    _keyExtractor = (info, index) => info.id;

    placePressedHandler = () => {
        alert('Place was pressed!');
    };

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    style={styles.itemsList}
                    data={this.props.place}
                    keyExtractor={this._keyExtractor}
                    renderItem={(info) => (
                        <PlaceItem
                            placeName={info.item.data.title}
                            placeImage={{uri: info.item.data.thumbnail}}
                            id={info.item.data.id}
                            placePressed={this.placePressedHandler}
                        />
                    )}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 30
    },
    input: {
        width: '80%',
        height: 40
    },
    largeText: {
        fontSize: 20
    },
    inputContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start'
    },
    itemsList: {
        width: '100%'
    }
});

const mapStateToProps = state => {
    return {
        place: state.place
    }
};

const mapDispatchToProps = dispatch => {
    return {
        addPlace: () => dispatch(addPlace())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);