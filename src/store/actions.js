import axios from "axios/index";

export const ADD_PLACE = 'ADD_PLACE';
export const FETCH_TODO_ERROR = 'FETCH_TODO_ERROR';



export const fetchTodoSuccess = (place) => {
    return {type: ADD_PLACE, place};
};

export const fetchTodoError = () => {
    return {type: FETCH_TODO_ERROR};
};


export const addPlace = () => {
    return dispatch => {
        axios.get('/pics.json').then(response => {
            // console.log(response.data);
            if (response.data) {
                dispatch(fetchTodoSuccess(response.data.data.children))
            };
        }, error => {
            dispatch(fetchTodoError());
        });
    }
};