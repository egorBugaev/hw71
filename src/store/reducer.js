import {ADD_PLACE} from "./actions";

const initialState = {
  place: []

};

const reducer = (state = initialState, action) => {
  switch (action.type) {
      case ADD_PLACE:
        return {
        ...state,
        place: action.place
      };
    default:
      return state;
  }
};

export default reducer;